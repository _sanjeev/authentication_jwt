const { BadRequestError, UnAuthorizeError } = require("../errors");
const jwt = require("jsonwebtoken");
const { StatusCodes } = require("http-status-codes");

module.exports.login = (req, res) => {
    const { username, password } = req.body;
    if (!username) throw new BadRequestError("Username is mandatory!");
    if (!password) throw new BadRequestError("Password is mandatory!");

    const id = new Date().getDate();

    const token = jwt.sign(
        {
            id,
            username,
        },
        "jwtSecret",
        {
            expiresIn: "30d",
        }
    );

    return res.status(StatusCodes.OK).json({
        status: true,
        message: "Successfully login!",
        entity: token,
    });
};

module.exports.dashboard = (req, res) => {
    const { id, username } = req.user;
    return res.status(StatusCodes.OK).json({
        status: true,
        message: "Successfully login!",
        entity: {
            id,
            username,
        },
    });
};
