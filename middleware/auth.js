const { UnAuthorizeError } = require("../errors");
const jwt = require("jsonwebtoken");

module.exports.auth = (req, res, next) => {
    const authHeaders = req.headers.authorization;

    if (!authHeaders || !authHeaders.startsWith("Bearer ")) throw new UnAuthorizeError("Unauthorize user!");

    const token = authHeaders.split(" ")[1];

    try {
        const decoded = jwt.verify(token, "jwtSecret");
        const { id, username } = decoded;
        req.user = {
            id,
            username,
        };
        next();
    } catch (error) {
        throw new UnAuthorizeError("Unauthorize user!");
    }
};
