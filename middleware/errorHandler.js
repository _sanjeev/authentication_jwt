const { CustomApiError } = require("../errors");
const { StatusCodes } = require("http-status-codes");

module.exports.errorHandler = (err, req, res, next) => {
    if (err instanceof CustomApiError) {
        return res.status(err.statusCode).json({
            status: err.status,
            message: err.message,
        });
    }
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        status: false,
        message: err,
    });
};
