const { StatusCodes } = require("http-status-codes");

module.exports.notFound = (req, res, next) => {
    return res.status(StatusCodes.NOT_FOUND).json({
        status: false,
        message: "Resource Not Found!",
    });
};
