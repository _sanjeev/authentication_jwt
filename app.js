const express = require("express");
const { errorHandler } = require("./middleware/errorHandler");
const { notFound } = require("./middleware/notFound");
require("dotenv").config();
require("express-async-errors");
const app = express();

app.use(express.json());
app.use("/", require("./routes"));
app.use(notFound);
app.use(errorHandler);

const port = process.env.PORT || 5000;

const start = async () => {
    app.listen(port, (err) => {
        if (err) {
            console.log("Error ", err);
            return;
        }
        console.log(`Server is listening on port ${port}`);
    });
};

start();
