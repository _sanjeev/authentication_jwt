const { BadRequestError } = require("./BadRequestError");
const { CustomApiError } = require("./CustomApiError");
const { UnAuthorizeError } = require("./UnAuthorizeError");

module.exports = {
    BadRequestError,
    CustomApiError,
    UnAuthorizeError,
};
