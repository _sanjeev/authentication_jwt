const { CustomApiError } = require("./CustomApiError");
const { StatusCodes } = require("http-status-codes");
class UnAuthorizeError extends CustomApiError {
    constructor(message) {
        super(message);
        this.status = false;
        this.statusCode = StatusCodes.UNAUTHORIZED;
    }
}

module.exports = {
    UnAuthorizeError,
};
