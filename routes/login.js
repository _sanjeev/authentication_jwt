const express = require("express");
const { login, dashboard } = require("../controllers/auth");
const { auth } = require("../middleware/auth");
const router = express.Router();

router.post("/login", login);
router.get("/dashboard", auth, dashboard);

module.exports = router;
